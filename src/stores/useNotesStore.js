import { defineStore } from "pinia";

export const useNotesStore = defineStore("notesStore", {
  state: () => {
    return {
      notes: [
        {
          id: "id1",
          content: "Fugiat consequat in culpa pariatur nulla amet.",
        },
        {
          id: "id12",
          content:
            "Officia magna sit duis consectetur occaecat nostrud cillum esse laboris.",
        },
        {
          id: "id3",
          content:
            "Aliquip aliqua reprehenderit aliquip eiusmod aliqua sint ullamco eiusmod ea nulla.",
        },
      ],
    };
  },
  actions: {
    addNote(newNoteContent) {
      const currentDate = new Date().getTime();
      const id = currentDate.toString();

      let note = {
        id,
        content: newNoteContent,
      };

      this.notes.unshift(note);
    },
    deleteNote(id) {
      this.notes = this.notes.filter((note) => note.id !== id);
    },
    updateNote(id, content) {
      const index = this.notes.findIndex((note) => note.id === id);
      this.notes[index].content = content;
    },
  },
  getters: {
    getNoteContent: (state) => {
      return (id) => state.notes.filter((note) => note.id === id)[0].content;
    },
    totalNotesCount: (state) => {
      return state.notes.length;
    },
    totalCharactersCount: (state) => {
      let count = 0;
      state.notes.forEach((note) => (count += note.content.length));
      return count;
    },
  },
});
